#!/usr/bin/python
import sys
import re
import copy 
import itertools
from decimal import Decimal

queries=[]
chance_nodes=[]
decision_nodes=[]
utility_node={}

def parseArguments():

        input_file= sys.argv[2]
        fp=open(input_file,'r')
        input_content=fp.read()
        fp.close()
        return input_content

def parseQueries(input_content):
	
	query_section=input_content.split("******")[0].split("\n")
	if "" in query_section:
		query_section.remove("")
        query_section=[x.rstrip() for x in query_section]
        for query_line in query_section:
                query={}
                query['type']=query_line.split("(")[0]
                variables=query_line.split("(")[1].split(")")[0].split(" | ")[0].split(", ")
		if "" in variables:
			variables.remove("")
                vars=[]
                for var in variables:
                        variable={}
                        variable['event']=var.split(" = ")[0]
                        if len(var.split(" = "))>1:
                                variable['truth']=var.split(" = ")[1]
                        else:
                                variable['truth']='unknown'
                        vars.append(variable)   
                query['variables']=vars
                if(len(query_line.split("(")[1].split(")")[0].split(" | "))>1):
                        parents_list=query_line.split("(")[1].split(")")[0].split(" | ")[1].split(", ")
			if "" in parents_list:
				parents_list.remove("")
                        parents=[]
                        for val in parents_list:
                                parent={}
                                parent['event']=val.split(" = ")[0]
                                if len(val.split(" = "))>1:
                                        parent['truth']=val.split(" = ")[1]
                                else:   
                                        parent['truth']='unknown'
                                parents.append(parent)
                	query['parents']=parents
                else:
                        query['parents']=['none']
                queries.append(query)

def parseUtilityNode(network_section):
        utility=network_section[1]
        utility_node['type']='utility'
	utility_node['event']='utility'
        if len(utility.split(" | "))>1:
        	table=[]
                parents=utility.split(" | ")[1].split("\n")[0].split(" ")
		if "" in parents:
			parents.remove("")
                utility_node['parents']=parents
                lines=utility.split(" | ")[1].split("\n")[1:]
		if "" in lines:
			lines.remove("")
                for line in lines:
  	              row={}
                      row['utility']=float(line.split(" ")[0])
                      truth_values=line.split(" ")[1:]
                      for i in range(len(truth_values)):
 	                     row[parents[i]]=truth_values[i]
                      table.append(row)
                utility_node['table']=table
        else:
        	utility_node['parents']=["none"]
                row={}
                row['utility']=float(utility.split("\n")[1])
                utility_node['table']=row

def parseDecisionNode(section):
	node={}
	node['type']='decision'
        node['event']=section.split("\n")[0].split(" | ")[0]
        node['parents']=['none']
        node['table']='N/A'
	node['truth']='unknown'
        decision_nodes.append(node)

def parseNetwork(input_content):
	
	network_section=input_content.split("******")[1:]
	if "" in network_section:
		network_section.remove("")
	if len(network_section)>1:
		parseUtilityNode(network_section)
	elements=network_section[0].split("***")
	elements=[x.lstrip() for x in elements]
	if "" in elements:
		elements.remove("")
	for section in elements:
		node={}
		if section.split("\n")[1]=="decision":
			parseDecisionNode(section)			
		else:
			lines=section.split("\n")
			if "" in lines:
				lines.remove("")
			node['event']=lines[0].split(" | ")[0]
			node['type']='chance'
			node['truth']="+"
			if len(lines[0].split(" | "))>1:
				parents=lines[0].split(" | ")[1].split(" ")
				if "" in parents:
					parents.remove("")
				node['parents']=parents
				table=[]
				for line in lines[1:]:
					row={}
					truth_values=line.split(" ")
					for i in range(len(truth_values)):
						if i==0:
							row[node['event']]=float(truth_values[i])
						else:
							row[parents[i-1]]=truth_values[i]
					table.append(row)
				node['table']=table
			else:
				node['parents']=['none']
				node['table']=float(lines[1])
			chance_nodes.append(node)
				
def extractParameters():

        input_content=parseArguments()
	parseQueries(input_content)
	parseNetwork(input_content)

def generatePermutations(length):
	
	return list(itertools.product(["+", "-"],repeat=length))	

def query_vars_extended(X,permutation):
	nvars=len(X)
	node_list=[]
	for i in range(nvars):
		node_copy={}
		node_copy=copy.deepcopy(X[i])
		node_copy['truth']=permutation[i]
		node_list.append(node_copy)
	return node_list
		

def inEvidence(var,e):
	
	for elem in e:
		if elem=='none':
			continue
		if elem['event']==var['event']:
			return True
	return False

def compute_prob(var,e):
	
	keys=[]
	for node in chance_nodes:
		if node['event']==var:
			parents=node['parents']
			for parent in parents:
				for elem in e:
					if elem=='none': continue
					if elem['event']==parent:
						keys.append({parent:elem['truth']})
			table=node['table']
			break
	nkeys=len(keys)
	matched=len(keys)
	found=False
	if matched==0:
		prob=table
	else:
		for row in table:
			if found: break
			for key in keys:
				if found: break
				for k in key:
					if row[k]==key[k]:
						matched-=1
						if matched==0:
							prob=row[var]
							found=True
			matched=nkeys
	for elem in e:
		if elem=='none':continue
		if elem['event']==var:
			if elem['truth']=='+': return prob
			else: return float(1-prob)
					

def enumerate_all(vars,e):
	
	if len(vars)==0:
		return 1.0
	var=vars[0]
	if inEvidence(var,e):
		return compute_prob(var['event'],e)*enumerate_all(vars[1:],e)
	else:
		e_plus=e+query_vars_extended([var],("+",))
		e_minus=e+query_vars_extended([var],("-",))
		return compute_prob(var['event'],e_plus)*enumerate_all(vars[1:],e_plus)+compute_prob(var['event'],e_minus)*enumerate_all(vars[1:],e_minus)

def inMarked(node,marked):

	for elem in marked:
		if elem==node['event']:
			return True
	return False

def visit(node,marked,sorted,nodes_copy):
	
	if inMarked(node,marked)==False:
		for elem in nodes_copy:
			if node['event'] in elem['parents']:
				visit(elem,marked,sorted,nodes_copy)
		marked.add(node['event'])
		sorted.append(node)
	
def sort():

	nodes_copy=copy.deepcopy(chance_nodes)
	nvars=len(nodes_copy)
	sorted=[]
	marked=set()
	while len(marked)<nvars:
		for node in nodes_copy:
			if inMarked(node,marked)==False:
				visit(node,marked,sorted,nodes_copy)		
	sorted.reverse()
	return sorted

def normalize(distribution,X):

	truth=""
	prob=float(0)
	for val in X:
		truth=truth+val['truth']
	found=False
	for entry in distribution:
		if found: break
		for key in entry:
			if key==truth:
				prob=entry[key]
				found=True
	sum=float(0)
	for entry in distribution:
		for key in entry:
			sum+=entry[key]
	return (prob/sum)

def checkAncestor(variable,node):
	
	if node=='none' : return False
	if node['parents']==['none']: return False
	if variable in node['parents']: return True
	for elem in node['parents']:
		ancestor=True
		parent={}
		for n in chance_nodes:
			if elem=='none':
				continue
			if n['event']==elem:
				parent=n
				break
		ancestor=checkAncestor(variable,parent)
		if ancestor==True:
			return True
	return False

def isAncestor(variable,e):
	
	enodes=[]
	nodes_copy=copy.deepcopy(chance_nodes)
	for elem in e:
		for node in nodes_copy:
			if elem=='none': break
			if node['event']==elem['event']:
				enodes.append(node)
	for node in enodes:
		ancestor=checkAncestor(variable,node)
		if ancestor==True:
			return True
	return False

def independentNodes(X,e):
	
	nodes_copy=copy.deepcopy(chance_nodes)
	nodes=[]
	for var in X:
		for node in nodes_copy:
			if node['event']==var['event']:
				node['truth']=var['truth']
				nodes.append(node)
				if isAncestor(node['event'],e):
					return (0,False)
	indep=0
	for var in nodes:
		if var['parents']==['none']:
			indep+=1
	if indep==len(X):
		prob=float(1)
		for var in nodes:
			if var['truth']=='+':
				prob*=var['table']
			else:
				prob*=(1-var['table'])
		return (prob,True)
	return (0,False) 

def enumeration_ask(X,e):

	indep_prob=independentNodes(X,e)
	if indep_prob[1]==True:
		return indep_prob[0]	
	distribution=[]
	truth_permutations=generatePermutations(len(X))
	sorted_nodes=sort()	
	for permutation in truth_permutations:
		e_copy={}
		e_copy=copy.deepcopy(e)
		e_copy=e_copy+query_vars_extended(X,permutation)
		distribution.append({"".join(permutation):enumerate_all(sorted_nodes,e_copy)})
	return normalize(distribution,X)

def extendDecisionNodes(nodes):
	
	decision_nodes_copy=copy.deepcopy(decision_nodes)
	for node in nodes:
		for dec in decision_nodes_copy:
			if node['event']==dec['event']:
				dec['truth']=node['truth']
	return decision_nodes_copy

def compute_utility(x,e):
	
	parents=utility_node['parents']
	keys=[]
	for elem in x:
		if elem['event'] in parents:
			keys.append({elem['event']:elem['truth']})
	for elem in e:
		if elem=='none': continue
                if elem['event'] in parents:
                        keys.append({elem['event']:elem['truth']})
	
	table=utility_node['table']
	found=False
	matched=len(keys)
	nkeys=matched
	if matched==0:
		return table
	for row in table:
		if found:
			break
		for entry in keys:
			if found: break
			for key in entry:
				if entry[key]==row[key]:
					matched-=1
					if matched==0:
						util=row['utility']
						found=True
						break
		matched=nkeys
	return util

if __name__ == "__main__":
	
	extractParameters()
	out_fp=open("output.txt","w")
	out_fp.close()
	out_fp=open("output.txt","a")
	for query in queries:
		if query['type']=='P':
			probability=enumeration_ask(query['variables'],query['parents'])
			probability=str(Decimal(str(probability)).quantize(Decimal('.01')))
			out_fp.write(probability+"\n")
		elif query['type']=='EU':
			chance_nodes_copy=copy.deepcopy(chance_nodes)
			parents=utility_node['parents']
			X=[]
			for node in chance_nodes_copy:
				for parent in parents:
					if node['event']==parent:
						X.append(node)
			decision_nodes_extended=extendDecisionNodes(query['variables'])
			e=query['parents']+decision_nodes_extended
			truth_permutations=generatePermutations(len(X))
			expected_utility=float(0)
			for permutation in truth_permutations:
				x=query_vars_extended(X,permutation)
				probability=enumeration_ask(x,e)
				expected_utility+=probability*compute_utility(x,e)
			expected_utility=int(round(expected_utility))
			out_fp.write(str(expected_utility)+"\n")
		elif query['type']=='MEU':
			chance_nodes_copy=copy.deepcopy(chance_nodes)
                        parents=utility_node['parents']
			ordered_decision_nodes=[]
			decision_nodes_copy=copy.deepcopy(decision_nodes)
			for var in query['variables']:
				for node in decision_nodes_copy:
					if var['event']==node['event']:
						ordered_decision_nodes.append(node)
                        X=[]
                        for node in chance_nodes_copy:
                                for parent in parents:
                                        if node['event']==parent:
                                                X.append(node)
			decision_perms=generatePermutations(len(ordered_decision_nodes))
			max_expected_utility=float("-inf")
			for perm in decision_perms:
				decision_nodes_extended=query_vars_extended(ordered_decision_nodes,perm)
                        	e=query['parents']+decision_nodes_extended
                       		truth_permutations=generatePermutations(len(X))
                        	expected_utility=float(0)
                        	for permutation in truth_permutations:
                                	x=query_vars_extended(X,permutation)
                                	probability=enumeration_ask(x,e)
                                	expected_utility+=probability*compute_utility(x,e)
				if expected_utility>max_expected_utility:
					max_expected_utility=expected_utility
					max_perm=perm
			for i in range(len(max_perm)):
				out_fp.write(max_perm[i]+" ")
			out_fp.write(str(int(round(max_expected_utility)))+"\n")
	out_fp.seek(-1,1)
	out_fp.truncate()
        out_fp.close()
					
